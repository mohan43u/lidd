package main

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/mohan43u/lidd/mod/lidd"
)

func main() {
	statechannel := lidd.ListenLidState()
	if statechannel == nil {
		fmt.Println("main: initialization failed")
		return
	}
	for {
		select {
		case state, ok := <-statechannel:
			if ok == false {
				fmt.Println("no more data")
				return
			}
			fmt.Println(state)
			if state == "closed" {
				cmd := exec.Command(os.Args[1], os.Args[2:]...)
				cmd.Stdin = os.Stdin
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				cmd.Run()
			}
		}
	}
}
