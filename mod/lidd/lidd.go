package lidd

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func ListenLidState() chan string {
	file := "/proc/acpi/button/lid/LID0/state"
	statechannel := make(chan string)
	var oldstate string

	go func() {
		ticker := time.Tick(1 * time.Second)
		for tick := range ticker {
			contents, error := os.ReadFile(file)
			if error != nil {
				fmt.Println(tick, "Failed to read", file, error)
				close(statechannel)
				return
			}
			lines := strings.Split(string(contents), "\n")
			for _, line := range lines {
				if strings.HasPrefix(line, "state:") == false {
					continue
				}
				columns := strings.Split(line, ":")
				state := strings.TrimSpace(columns[1])
				if oldstate != state {
					statechannel <- state
				}
				oldstate = state
			}
		}
	}()
	return statechannel
}
