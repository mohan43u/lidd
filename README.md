### Lidd Daemon which listens for lid changes

When started, it listens for lid open/closed event and execute command if lid closed

### syntex

```
lidd <shell command>
```
